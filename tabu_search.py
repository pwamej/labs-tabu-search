import random
import math
import numpy as np

from evaluation import energy, merit, value_flip
from mutation import flip

def tabu_search(s):
  L = len(s)
  T = create_T(s)
  C = create_C(T)
  max_iters = round(random.uniform(0.5 * L, 1.5 * L))
  M = [0 for _ in range(L)]
  minTabu = max_iters / 10
  extraTabu = max_iters / 50
  sa = s
  fa = energy(s)
  for k in range(max_iters):
    fc = math.inf
    for i in range(L):
      sp = flip(s, i)
      fp = value_flip(s, i, T, C)
      if k >= M[i] or fp < fa:
        if fp < fc:
          fc = fp
          sc = sp
          ic = i
    s = sc
    T = create_T(s)
    C = create_C(T)
    M[ic] = k + minTabu + random.uniform(0, extraTabu)
    if fc < fa:
      sa = sc
      fa = fc
  return sa

def create_T(s):
  L = len(s)
  T = np.zeros((L-1, L-1))
  for i in range(1,L):
    for j in range(L-i):
      T[i-1,j] = s[j] * s[i+j]
  return T

def create_C(T):
  return np.array(list(map(sum, T)))
