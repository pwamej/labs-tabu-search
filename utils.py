import itertools

def partition(items, predicate=bool):
  """Splits collection into tuple of two lists,
  first containing items that satisfy given predicate and second containing the rest."""
  return (list(filter(predicate, items)),
          list(filter(lambda x: not predicate(x), items)))
