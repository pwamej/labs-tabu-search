import numpy as np

# Reverse i-th element in binary sequence s
def flip(s, i):
  return np.hstack((s[:i], [-s[i]], s[i+1:]))
