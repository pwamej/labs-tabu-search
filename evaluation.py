from typing import List
import numpy as np

def energy(s: np.array):
  return sum([_correl(s, k) ** 2 for k in range(1, len(s))])

def merit(s: np.array):
  return (len(s) ** 2) / (2 * energy(s))

# Recompute fitness of sequence flip(s, i)
def value_flip(s, i, T, C):
  f = 0
  L = len(s)
  for p in range(L-1):
    v = C[p]
    if p < L - i - 1:
      v -= 2 * T[p,i]
    if p < i:
      v -= 2 * T[p,i-p-1]
    f += v ** 2
  return f

def _correl(s: np.array, k: int):
  return sum([s[i] * s[i+k] for i in range(len(s) - k)])
