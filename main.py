import csv
import random
import time
from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np

from evaluation import energy
from tabu_search import tabu_search

BASE_TIMEOUT_S = 60  # for L <= 30
EXTRA_TIMEOUT_S = 10  # for each bit above 30
ITERATIONS = 1


def str2arr(str_: str) -> np.array:
    return np.array([1 if x == "1" else -1 for x in str_])


# Execute tabu search for sequence of length len(opt) until optimum opt is found or time limit is reached
# returns tuple (best_seq, time_elapsed), time_elapsed in seconds
def run_tabu(opt: np.array) -> Tuple[np.array, float]:
    L = len(opt)
    min_energy = energy(opt)
    max_time = time_limit(L)
    start = time.time()
    time_elapsed = 0
    while time_elapsed < max_time:
        start_seq = np.array([-1 if random.random() <= 0.5 else 1 for _ in range(L)])
        found_seq = tabu_search(start_seq)
        time_elapsed = time.time() - start
        if energy(found_seq) == min_energy:
            return found_seq, time_elapsed
    return found_seq, time_elapsed


# calculate max time in seconds for given sequence length L
def time_limit(L: int):
    if L <= 30:
        return BASE_TIMEOUT_S
    else:
        return BASE_TIMEOUT_S + (L - 30) * EXTRA_TIMEOUT_S


# load optimal LABS with length up to L
# if L is not provided, load all sequences
def load_labs(L: int = None) -> np.array:
    labs = []
    with open('labs.txt') as csvfile:
        labs_csv = csv.DictReader(csvfile, delimiter='\t')
        for row in labs_csv:
            if int(row['L']) > L:
                break
            labs.append(str2arr(row['coordinate']))
    return labs


if __name__ == "__main__":
    labs = load_labs(60)  # load optimal LABS with length up to N
    lengths = list(map(len, labs))
    count = len(lengths)
    all_run_times = np.zeros((ITERATIONS, count))
    all_distances_to_optimum = np.zeros((ITERATIONS, count))
    for i in range(ITERATIONS):
        run_times = []
        distances_to_optimum = []
        for opt_seq in labs:
            found_seq, time_elapsed = run_tabu(opt_seq)
            run_times.append(round(time_elapsed, 2))
            distance = abs(energy(found_seq) - energy(opt_seq)) / energy(opt_seq) * 100
            distances_to_optimum.append(distance)
        all_run_times[i] = run_times
        all_distances_to_optimum[i] = distances_to_optimum

    run_times = np.mean(all_run_times, axis=0)
    times_error = np.std(all_run_times, axis=0)
    distances_to_optimum = np.mean(all_distances_to_optimum, axis=0)
    distances_error = np.std(all_distances_to_optimum, axis=0)

    # Mark timed out executions
    colors = ['b' if t < time_limit(l) else 'r' for (l, t) in zip(lengths, run_times)]

    ax1 = plt.subplot(211)
    plt.scatter(lengths, run_times, c=colors)
    plt.errorbar(lengths, run_times, yerr=times_error, fmt='none', zorder=0, marker='none', capsize=3)
    plt.ylabel('Time to find optimum [s]')

    plt.subplot(212, sharex=ax1)
    plt.errorbar(lengths, distances_to_optimum, yerr=distances_error, fmt='none', zorder=0, marker='none', capsize=3)
    plt.scatter(lengths, distances_to_optimum, c=colors)
    plt.xlabel('Sequence length')
    plt.ylabel('Relative distance to optimum (in energy) [%]')

    plt.show()
